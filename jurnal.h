#ifndef JURNAL_H
#define JURNAL_H

#include <QDialog>
#include <QStandardItemModel>
#include <QStandardItem>
#include "string.h"

using namespace std;
namespace Ui {
class jurnal;
}
struct jurnalpoc{
     string FIO;
     string nomer;
     string telefon;
     string parol;
     string login;
     string cpok;
};

class jurnal : public QDialog
{
    Q_OBJECT

public:
    explicit jurnal(QWidget *parent = nullptr);
    ~jurnal();

private:
    Ui::jurnal *ui;
    QStandardItemModel *tabliza;
};

#endif // JURNAL_H
