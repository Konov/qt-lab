#ifndef POC_H
#define POC_H

#include <QDialog>

namespace Ui {
class poc;
}

class poc : public QDialog
{
    Q_OBJECT

public:
    explicit poc(QWidget *parent = nullptr);
    ~poc();

private:
    Ui::poc *ui;
};

#endif // POC_H
