#include "cot.h"
#include "ui_cot.h"
#include "newpoc.h"
#include "jurnal.h"
cot::cot(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::cot)
{
    ui->setupUi(this);
}


cot::~cot()
{
    delete ui;
}

void cot::on_pushButton_clicked()
{
    newpoc gop;
    gop.setModal(true);
    gop.exec();
}

void cot::on_pushButton_3_clicked()
{
    jurnal gop;
    gop.setModal(true);
    gop.exec();
}
