#ifndef NEWPOC_H
#define NEWPOC_H

#include <QDialog>
using namespace std;
namespace Ui {
class newpoc;
}

class newpoc : public QDialog
{
    Q_OBJECT

public:
    explicit newpoc(QWidget *parent = nullptr);
    ~newpoc();
    QString FIO;
    QString telefon;
    QString login;
    QString parol;
    QString nomer;
    QString cpok;
    static int id (string new_file);
private slots:
    void on_lineEdit_textEdited(const QString &arg1);


    void on_lineEdit_2_textEdited(const QString &arg1);

    void on_lineEdit_3_textEdited(const QString &arg1);

    void on_lineEdit_4_textEdited(const QString &arg1);

    void on_lineEdit_5_textEdited(const QString &arg1);

    void on_lineEdit_6_textEdited(const QString &arg1);

    void on_pushButton_clicked();

private:
    Ui::newpoc *ui;
};

#endif // NEWPOC_H
