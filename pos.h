#ifndef POS_H
#define POS_H

#include <QDialog>

namespace Ui {
class pos;
}

class pos : public QDialog
{
    Q_OBJECT

public:
    explicit pos(QWidget *parent = nullptr);
    ~pos();

private:
    Ui::pos *ui;
};

#endif // POS_H
