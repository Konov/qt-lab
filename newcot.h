#ifndef NEWCOT_H
#define NEWCOT_H

#include <QDialog>
using namespace std;
namespace Ui {
class newcot;
}

class newcot : public QDialog
{
    Q_OBJECT

public:
    explicit newcot(QWidget *parent = nullptr);
    ~newcot();
    QString FIO;
    QString telefon;
    QString login;
    QString parol;
    static int id (string new_file);

private slots:
    void on_lineEdit_textEdited(const QString &arg1);

    void on_lineEdit_2_textEdited(const QString &arg1);

    void on_lineEdit_3_textEdited(const QString &arg1);

    void on_lineEdit_4_textEdited(const QString &arg1);

    void on_pushButton_clicked();

private:
    Ui::newcot *ui;
};

#endif // NEWCOT_H
