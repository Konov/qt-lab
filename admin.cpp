#include "admin.h"
#include "ui_admin.h"
#include "newcot.h"
#include "jurnal.h"
admin::admin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::admin)
{
    ui->setupUi(this);
}

admin::~admin()
{

    delete ui;
}

void admin::on_pushButton_clicked()
{
    newcot gop;
    gop.setModal(true);
    gop.exec();
}

void admin::on_pushButton_3_clicked()
{
    jurnal gop;
    gop.setModal(true);
    gop.exec();
}
